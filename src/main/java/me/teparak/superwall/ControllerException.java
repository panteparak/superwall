package me.teparak.superwall;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.view.RedirectView;

@ControllerAdvice
public class ControllerException {

    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView redirectToRoot(){
        return new ModelAndView(new RedirectView("/", true));
    }
}
