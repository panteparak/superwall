package me.teparak.superwall.wall;

import me.teparak.superwall.db.SimpleStorage;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/wall")
public class WallController {

    @Autowired
    private SimpleStorage storage;

    @GetMapping("/{user}")
    public String getPostOf(Model model, @PathVariable("user") String user){
        model.addAttribute("posts", storage.getPostOf(user));
        return "wall";
    }

    @PostMapping("/{user}")
    public String addPostOf(Model model, @PathVariable("user") String user, @RequestParam("message") String message){
        if (StringUtils.isNotBlank(user) && StringUtils.isNotBlank(message)){
            try {
                storage.addPostTo(user, message);
            }catch (Exception e){
                return "redirect:/";
            }
        }

        model.addAttribute("posts", storage.getPostOf(user));
        return "wall";
    }
}
