package me.teparak.superwall;

import me.teparak.superwall.db.SimpleStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@EnableScheduling
public class SuperWallApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuperWallApplication.class, args);
    }

    @Autowired
    private SimpleStorage storage;

    @Scheduled(fixedRate = 7200000)
    public void reset(){
        storage.reset();
    }
}
