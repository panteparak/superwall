package me.teparak.superwall.user;

import me.teparak.superwall.db.SimpleStorage;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class UserController {

    @Autowired
    private SimpleStorage storage;

    @PostMapping
    public ModelAndView addUser(Model model, @RequestParam("user") String user){

        if (StringUtils.isNotBlank(user)){
            try {
                storage.addUser(user);
            } catch (Exception e){}
        }
        model.addAttribute("users", storage.getAllUser());
        return new ModelAndView("user", model.asMap());
    }

    @GetMapping
    public ModelAndView getUser(Model model){
        model.addAttribute("users", storage.getAllUser());
        return new ModelAndView("user", model.asMap());
    }
}
