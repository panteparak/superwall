package me.teparak.superwall.db;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SimpleStorage {
    private Map<String, List<String>> storage = new ConcurrentHashMap<>();


    public SimpleStorage() {
        populate();
    }

    public void addPostTo(String user, String message) throws UserNotExistException {
        synchronized (this){
            if (! storage.containsKey(user)) throw new UserNotExistException("User Not Exist");
        }
        storage.get(user).add(message);
    }

    public List<String> getPostOf(String user){
        List<String> temp = new ArrayList<>(storage.get(user));
        Collections.reverse(temp);
        return temp;
    }

    public void addUser(String user) throws UserExistException {
        storage.compute(user, (k, v) -> {
            if (v != null) throw new UserExistException("User Already Exist");
            return new LinkedList<>();
        });
    }

    public Set<String> getAllUser() {
        return storage.keySet();
    }

    public void reset(){
        storage = new ConcurrentHashMap<>();
        populate();
    }

    private void populate(){
        addUser("Hulk");
        addUser("Batman");
        addUser("Stan Lee");
        addPostTo("Hulk", "Marvel");
        addPostTo("Hulk", "Infinity War");
        addPostTo("Hulk", "Marvel");
        addPostTo("Batman", "Wayne");
    }

}
